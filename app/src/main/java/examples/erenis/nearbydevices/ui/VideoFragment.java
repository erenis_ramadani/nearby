package examples.erenis.nearbydevices.ui;

import android.app.Activity;
import android.app.Fragment;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;

import examples.erenis.nearbydevices.R;


/**
 * Created by Erenis on 12/25/2014.
 */
public class VideoFragment extends Fragment implements View.OnClickListener
{
    public static final String TAG = "SM.VideoFragment";

    private MainActivity mActivity;

    private VideoRenderer.Callbacks mLocalRender;
    private VideoRenderer.Callbacks mRemoteRender;

    private RelativeLayout mVideoView;
    private GLSurfaceView mVideoRenderView;

    private Button mButton;

    public VideoFragment()
    {
    }

    @Override
    public void onAttach(Activity activity)
    {
        mActivity = (MainActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mVideoView = (RelativeLayout) inflater.inflate(R.layout.video_interface, null, false);

        mVideoRenderView = (GLSurfaceView) mVideoView.findViewById(R.id.glView);

        mVideoRenderView.setRenderer((GLSurfaceView.Renderer) mLocalRender);
        mButton = (Button) mVideoView.findViewById(R.id.hangupButton);
        if (mActivity.isHost())
        {
            mButton.setVisibility(View.VISIBLE);
            mButton.setOnClickListener(this);
        }

        VideoRendererGui.setView(mVideoRenderView, new Runnable()
        {
            @Override
            public void run()
            {

            }
        });
        mLocalRender = mActivity.getmLocalRender();
        mRemoteRender = mActivity.getmRemoteRender();
        return mVideoView;
    }

    @Override
    public void onDestroyView()
    {
        mVideoView.removeView(mVideoRenderView);
        super.onDestroyView();
    }


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v)
    {
        int id = v.getId();
        switch (id)
        {
            case R.id.hangupButton:
                mActivity.stopVideo();
        }
    }
}
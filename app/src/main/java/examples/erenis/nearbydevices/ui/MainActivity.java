package examples.erenis.nearbydevices.ui;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AppIdentifier;
import com.google.android.gms.nearby.connection.AppMetadata;
import com.google.android.gms.nearby.connection.Connections;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnection;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;
import org.webrtc.VideoRenderer;
import org.webrtc.VideoRendererGui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import examples.erenis.nearbydevices.R;
import examples.erenis.nearbydevices.utils.AppRTCAudioManager;
import examples.erenis.nearbydevices.utils.PeerConnectionClient;
import examples.erenis.nearbydevices.utils.PeerConnectionClient.PeerConnectionEvents;
import examples.erenis.nearbydevices.utils.SignalingParameters;

public class MainActivity extends AppCompatActivity implements Connections.EndpointDiscoveryListener,
        Connections.MessageListener, Connections.ConnectionRequestListener, /*VideoFragmentListener,*/ PeerConnectionEvents
{

//AIzaSyCDhbcEeWHwW3mmabj2NeC8pnr1FbmxN7c

    private static String LOG_TAG = MainActivity.class.getSimpleName();

    GoogleApiClient client;

    Map<String, String> mDevicesList = new HashMap<>();

    private String mLocalDevice;

    private String mRemoteDevice;

    private String mServiceId;

    private boolean isHost;

    ListView mNearByDevices;

    private ArrayAdapter<String> mAdapter;

    private ProgressDialog mProgressDialog;

    private Button mStartCallButton;

    private TextView mTextView;

    private VideoFragment mVideoFragment;

    private PeerConnectionClient peerConnection;

    VideoRenderer.Callbacks mLocalRender;

    VideoRenderer.Callbacks mRemoteRender;

    AppRTCAudioManager audioManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        client = new GoogleApiClient.Builder(this).addApi(Nearby.CONNECTIONS_API).addConnectionCallbacks(mConnectionCallback)
                                                  .addOnConnectionFailedListener(mConnectionCallbackFailed).build();
        GLSurfaceView glSurfaceView = new GLSurfaceView(this);

        VideoRendererGui.setView(glSurfaceView, new Runnable()
        {
            @Override
            public void run()
            {

            }
        });
        mVideoFragment = new VideoFragment();
        setHost();
        initOtherViews();
    }

    private void setHost()
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(R.string.am_I_master);
        dialog.setCancelable(false);
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                isHost = false;
//                startAdvertising();
                mTextView.setVisibility(View.VISIBLE);
                client.connect();
                dialog.dismiss();
            }
        });
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                isHost = true;
//                startDiscovery();
                mNearByDevices = (ListView) findViewById(R.id.widget_nearby_devices_list);
                mNearByDevices.setVisibility(View.VISIBLE);
                findViewById(R.id.widget_list_descriptor_text).setVisibility(View.VISIBLE);
                mAdapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1);
                mNearByDevices.setAdapter(mAdapter);
                mNearByDevices.setOnItemClickListener(mOnItemClickListener);
                client.connect();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void initOtherViews()
    {
        mStartCallButton = (Button) findViewById(R.id.widget_start_call_button);
        mTextView = (TextView) findViewById(R.id.widget_advertising_text);

        mStartCallButton.setOnClickListener(mOnClickListener);
    }

    private boolean isDeviceConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return info != null && info.isConnectedOrConnecting();
    }

    // Taken from Google's example
    private void startAdvertising()
    {
        if (!isDeviceConnected())
        {
            return;
        }

        // Advertising with an AppIdentifer lets other devices on the network discover
        // this application and prompt the user to install the application.
        List<AppIdentifier> appIdentifierList = new ArrayList<>();
        appIdentifierList.add(new AppIdentifier(getPackageName()));
        AppMetadata appMetadata = new AppMetadata(appIdentifierList);

        // Advertise for Nearby Connections. This will broadcast the service id defined in
        // AndroidManifest.xml. By passing 'null' for the name, the Nearby Connections API
        // will construct a default name based on device model such as 'LGE Nexus 5'.
        Nearby.Connections.startAdvertising(client, null, appMetadata, 0, this)
                          .setResultCallback(new ResultCallback<Connections.StartAdvertisingResult>()
                          {
                              @Override
                              public void onResult(@NonNull Connections.StartAdvertisingResult result)
                              {
                                  Log.d(LOG_TAG, "startAdvertising:onResult:" + result.getStatus());
                              }
                          });
    }

    /**
     * Begin discovering devices advertising Nearby Connections, if possible.
     */
    private void startDiscovery()
    {
        if (!isDeviceConnected())
        {
            return;
        }

        // Discover nearby apps that are advertising with the required service ID.

        Nearby.Connections.startDiscovery(client, mServiceId, 0, this).setResultCallback(new ResultCallback<Status>()
        {
            @Override
            public void onResult(@NonNull Status status)
            {
                Log.d(LOG_TAG, "startDiscovery: " + status.getStatus());
            }
        });
    }

    public void showVideoFragment()
    {
        Log.d(LOG_TAG, "showVideoFragment");
        if (!mVideoFragment.isAdded())
        {
            Fragment prior = getFragmentManager().findFragmentByTag("VideoFragment");
            if (prior != null)
            {
                Log.e(LOG_TAG, "showVideoFragment non null prior " + prior.isAdded());
            }
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, mVideoFragment, "VideoFragment")
                       .commit();
        }
    }

    @Override
    public void onEndpointFound(String endpointId, String deviceId, String serviceId, String endpointName)
    {
        Log.d(LOG_TAG, endpointId);
        mDevicesList.put(endpointName, endpointId);
        mAdapter.add(endpointName);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onEndpointLost(String s)
    {
        mDevicesList.remove(s);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onConnectionRequest(final String endpointId, String deviceId, String endpointName, byte[] payload)
    {
        mRemoteDevice = endpointId;
        Nearby.Connections.acceptConnectionRequest(client, endpointId, payload, MainActivity.this).setResultCallback(new ResultCallback<Status>()
        {
            @Override
            public void onResult(@NonNull Status status)
            {
                Log.d(LOG_TAG, "onAccept: " + status.getStatus());
                if (status.getStatus().isSuccess())
                {
                    mTextView.setText(R.string.waiting_to_start_call);
                }
                else
                {
                    Toast.makeText(MainActivity.this, R.string.something_happened, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            int id = v.getId();

            switch (id)
            {
                case R.id.widget_start_call_button:
                    createPeerConnectionWithParametersAndStart();
                    break;
                default:
                    break;
            }
        }
    };

    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
        {
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setMessage(getString(R.string.connecting));
            mProgressDialog.show();

            String device = mDevicesList.get(mAdapter.getItem(position));

            Log.d(LOG_TAG, "mRemoteDevice: " + device + ", mLocalDevice: " + mLocalDevice);
            Nearby.Connections.sendConnectionRequest(client, null, device, null, new Connections.ConnectionResponseCallback()
            {
                @Override
                public void onConnectionResponse(String s, Status status, byte[] bytes)
                {
                    Log.d(LOG_TAG, "response: " + status.getStatus());
                    mProgressDialog.dismiss();
                    if (status.getStatus().isSuccess())
                    {
                        mNearByDevices.setVisibility(View.GONE);
                        mStartCallButton.setVisibility(View.VISIBLE);
                        mTextView.setVisibility(View.VISIBLE);
                        mTextView.setText(getString(R.string.successfully_connected).concat(mAdapter.getItem(position)));
                    }
                }
            }, MainActivity.this);
        }
    };

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (client != null)
        {
            if (!isHost)
            {
                stopAdvertising();
            }
            else
            {
                stopDiscovery();
            }
            if (client.isConnected() || client.isConnecting())
            {
                client.disconnect();
            }
        }
    }

    private GoogleApiClient.ConnectionCallbacks mConnectionCallback = new GoogleApiClient.ConnectionCallbacks()
    {
        @Override
        public void onConnected(Bundle bundle)
        {
            Log.d(LOG_TAG, "Connected");
            mServiceId = getString(R.string.service_id);
            mLocalDevice = Nearby.Connections.getLocalDeviceId(client);

            if (isHost)
            {
                startDiscovery();
            }
            else
            {
                startAdvertising();
            }
        }

        @Override
        public void onConnectionSuspended(int i)
        {
            Toast.makeText(MainActivity.this, R.string.trying_to_reconnect, Toast.LENGTH_LONG).show();
            client.reconnect();
        }
    };

    private GoogleApiClient.OnConnectionFailedListener mConnectionCallbackFailed = new GoogleApiClient.OnConnectionFailedListener()
    {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
        {
            Log.d(LOG_TAG, "connection failed with errorCode " + connectionResult.getErrorCode());
        }
    };

    //
    @Override
    public void onMessageReceived(String s, byte[] bytes, boolean b)
    {
        String convertedBytes = new String(bytes);
        Log.d(LOG_TAG, "convertedBytes +  " + convertedBytes);
        JSONObject o = null;
        try
        {
            o = new JSONObject(convertedBytes);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        if (o != null)
        {
            String type = o.optString("type");
            String desc = o.optString("sdp");
            SessionDescription sdp = new SessionDescription(SessionDescription.Type.fromCanonicalForm(type), desc);
            if (isHost)
            {
                if (sdp.type.canonicalForm().toLowerCase().equals("answer"))
                {
                    peerConnection.setRemoteDescription(sdp);
                }
                else
                {
                    createPeerConnectionWithParametersAndStart();
                }
            }
            else
            {
                Log.d(LOG_TAG, "onMessage: " + convertedBytes);
                createPeerConnectionWithParametersAndStart();
                didReceiveRemoteDescription(convertedBytes);
            }
        }
    }

    @Override
    public void onDisconnected(String s)
    {
        Log.d(LOG_TAG, "onDisconnected");
    }

    @Override
    public void onLocalDescription(SessionDescription sdp)
    {
        JSONObject json = new JSONObject();
        jsonPut(json, "type", sdp.type.canonicalForm());
        jsonPut(json, "sdp", sdp.description);

        byte[] bytes = json.toString().getBytes();

        sendMessage(mRemoteDevice, bytes);
    }

    @Override
    public void onIceCandidate(IceCandidate candidate)
    {
        Log.d(LOG_TAG, "onIceCandidate");
    }

    @Override
    public void onIceConnected()
    {
        Log.d(LOG_TAG, "onIceConnected");
        showVideoFragment();
    }

    @Override
    public void onIceDisconnected()
    {
        Log.d(LOG_TAG, "onIceDisconnected");
    }

    @Override
    public void onPeerConnectionClosed()
    {
        Log.d(LOG_TAG, "onPeerConnectionClosed");
    }

    @Override
    public void onPeerConnectionStatsReady(StatsReport[] reports)
    {
        Log.d(LOG_TAG, "onPeerConnectionStatsReady");
    }

    @Override
    public void onPeerConnectionError(String description)
    {
        Log.d(LOG_TAG, "onPeerConnectionError");

        Toast.makeText(this, description, Toast.LENGTH_SHORT).show();
    }

    private static void jsonPut(JSONObject json, String key, Object value)
    {
        try
        {
            json.put(key, value);
        }
        catch (JSONException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void createPeerConnectionWithParametersAndStart()
    {
        Log.d(LOG_TAG, "createPeerConnectionWithParametersAndStart");
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                if (peerConnection == null)
                {
                    Log.d(LOG_TAG, "Creating peer connection factory");
                    final boolean initiator = true;

                    List<PeerConnection.IceServer> emptyList = new ArrayList<>();

                    MediaConstraints pcConstraints = new MediaConstraints();
                    // Mah: client must specify use of DTLS to parse XMS SDP
                    pcConstraints.optional.add(
                            new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));

                    MediaConstraints videoConstraints = new MediaConstraints();
                    videoConstraints.optional
                            .add(new MediaConstraints.KeyValuePair("maxWidth", "1280"));
                    videoConstraints.optional
                            .add(new MediaConstraints.KeyValuePair("maxHeight", "960"));
                    videoConstraints.optional
                            .add(new MediaConstraints.KeyValuePair("maxFrameRate", "30"));

                    MediaConstraints emptyConstraints = new MediaConstraints();

                    if (audioManager != null)
                    {
                        Log.i(LOG_TAG, "Initializing the audio manager...");
                        audioManager.init();
                    }

                    final boolean videoCallEnabled = true;
                    final boolean loopback = false;
                    final int videoWidth = 0;
                    final int videoHeight = 0;
                    final int videoFps = 0;
                    final int videoStartBitrate = 200; // kbps
                    final String videoCodec = "VP8";
                    final boolean videoCodecHwAcceleration = true;
                    final int audioStartBitrate = 20; // kbps
                    final String audioCodec = "opus";
                    final boolean noAudioProcessing = false;
                    final boolean cpuOveruseDetection = true;
                    PeerConnectionClient.PeerConnectionParameters peerConnectionParameters = new
                            PeerConnectionClient.PeerConnectionParameters(
                            videoCallEnabled, loopback, videoWidth, videoHeight, videoFps,
                            videoStartBitrate, videoCodec, videoCodecHwAcceleration,
                            audioStartBitrate, audioCodec, noAudioProcessing, cpuOveruseDetection);

                    SignalingParameters signalingParameters = new SignalingParameters(emptyList,
                            initiator,
                            pcConstraints,
                            videoConstraints,
                            emptyConstraints);

                    peerConnection = PeerConnectionClient.getInstance();
                    peerConnection.createPeerConnectionFactory(MainActivity.this,
                            VideoRendererGui.getEGLContext(),
                            peerConnectionParameters,
                            MainActivity.this);
                    mRemoteRender = VideoRendererGui.create(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FIT, false);
                    mLocalRender = VideoRendererGui.create(0, 0, 100, 100, VideoRendererGui.ScalingType.SCALE_ASPECT_FIT, false);
                    peerConnection.createPeerConnection(mLocalRender, mRemoteRender, signalingParameters);
                    if (isHost)
                    {
                        peerConnection.createOffer();
                    }
                }
                else
                {
                    Log.e(LOG_TAG, "");
                }
            }
        });
    }

    private void didReceiveRemoteDescription(String convertedBytes)
    {
        JSONObject json;
        try
        {
            json = new JSONObject(convertedBytes);
            String type = json.optString("type");
            String dsc = json.optString("sdp");
            SessionDescription remoteSDP = new SessionDescription(SessionDescription.Type.fromCanonicalForm(type), dsc);
            peerConnection.setRemoteDescription(remoteSDP);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    private void sendMessage(String destId, byte[] bytes)
    {
        Log.d(LOG_TAG, "sendMessage");
        Nearby.Connections.sendReliableMessage(client, destId, bytes);
    }

    private void stopAdvertising()
    {
        Nearby.Connections.stopAdvertising(client);
    }

    private void stopDiscovery()
    {
        Nearby.Connections.stopDiscovery(client, mServiceId);
    }

    public VideoRenderer.Callbacks getmLocalRender()
    {
        return mLocalRender;
    }

    public VideoRenderer.Callbacks getmRemoteRender()
    {
        return mRemoteRender;
    }

    public boolean isHost()
    {
        return isHost;
    }

    public void stopVideo()
    {
        peerConnection.stopVideoSource();
    }

}
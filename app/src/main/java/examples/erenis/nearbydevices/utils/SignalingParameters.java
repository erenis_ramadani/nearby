package examples.erenis.nearbydevices.utils;

import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnection;

import java.util.List;

public class SignalingParameters
{
	public final List<PeerConnection.IceServer> iceServers;
	public final boolean initiator;
	public final MediaConstraints pcConstraints;
	public final MediaConstraints videoConstraints;
	public final MediaConstraints audioConstraints;

	public SignalingParameters(
			List<PeerConnection.IceServer> iceServers,
			boolean initiator, MediaConstraints pcConstraints,
			MediaConstraints videoConstraints, MediaConstraints audioConstraints) {
		this.iceServers = iceServers;
		this.initiator = initiator;
		this.pcConstraints = pcConstraints;
		this.videoConstraints = videoConstraints;
		this.audioConstraints = audioConstraints;
	}
}
